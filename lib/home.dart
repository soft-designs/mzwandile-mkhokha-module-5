import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirstRoute extends StatelessWidget {
  // ignore: use_key_in_widget_constructors
  const FirstRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final firstNameController = TextEditingController();
    final lastNameController = TextEditingController();
    final ageController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Community Members'),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                controller: firstNameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'First Name',
                ),
              ),
              TextField(
                controller: lastNameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Last name',
                ),
              ),
              TextField(
                controller: ageController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Age',
                ),
              ),
              ElevatedButton(
                child: const Text('Add'),
                onPressed: () {
                  FirebaseFirestore.instance.collection('data').add({
                    'firstName': firstNameController.text.toString(),
                    'lastName': lastNameController.text.toString(),
                    'age': ageController.text.toString()
                  });
                },
              ),
              ElevatedButton(
                child: const Text('Show members'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const showMembers()),
                  );
                },
              ),
            ]),
      ),
    );
  }
}

class showMembers extends StatelessWidget {
  const showMembers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Community Members'),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('data').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            // ignore: curly_braces_in_flow_control_structures
            return ListView(
              children: snapshot.data!.docs.map((document) {
                return Card(
                  child: ListTile(
                    title: Text(document['lastName']),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const editMembers()),
                      );
                    },
                  ),
                );
                /*    return ListView(
            children: snapshot.data!.docs.map((document) {
              return Container(
                child: Container(
                    height: 50,
                    color: Colors.amber[600],
                    child: Center(
                        child: Text('First Name : ' +
                            document['firstName'] +
                            ' Last  Name : ' +
                            document['lastName'] +
                            ' Age : ' +
                            document['age']))),*/

                /* child: Center(
                    child: Text('First Name : ' +
                        document['firstName'] +
                        ' Last  Name : ' +
                        document['lastName'] +
                        ' Age : ' +
                        document['age'])),*/
                // );
              }).toList(),
            );
        },
      ),
    );
  }
}

class editMembers extends StatelessWidget {
  const editMembers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Community Members'),
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <
            Widget>[
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'First Name',
            ),
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Last name',
            ),
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Age',
            ),
          ),
          ElevatedButton(
            child: const Text('Add'),
            onPressed: () {},
          ),
          ElevatedButton(
            child: const Text('Update'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const showMembers()),
              );
            },
          ),
          ElevatedButton(
            child: const Text('Delete'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const showMembers()),
              );
            },
          ),
        ]),
      ),
    );
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  }
}
